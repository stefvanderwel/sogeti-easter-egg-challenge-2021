package nl.sogeti.com;

import nl.sogeti.logo.SogetiLogoDrawer;

import java.awt.*;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class EasterEggRunner {

    public static int SECONDS_IN_A_DAY = 24 * 60 * 60;

    public static void main(String[] args) {
        EasterEgg.drawEgg(createEggMetrics());

        // Please don't change the following code:
        new SogetiLogoDrawer().printSogetiLogo();
    }

    private static EggMetrics createEggMetrics() {
        List<AbstractMap.SimpleEntry<Point, String>> drawing = new ArrayList<>();
 
        drawing.addAll(Text.getTextForNumber(String.valueOf(getDaysUntilEaster()), new Point(48, 13), Color.WHITE));
        drawing.addAll(Text.getTextForNumber("DAYS", new Point(45, 19), Color.WHITE));
        
        Color startColor = new Color(43, 43, 111);
        for (int row = 0; row < 80; row++) {
            for (int col = 0; col < 44; col++) {
                drawing.add(new AbstractMap.SimpleEntry<>(new Point(row + 10, col), getGradientColor(startColor, row, col)));
            }
        }
        return new EggMetrics(30, 22, 50, 20, Colors.WHITE.getColor(), Colors.GREEN.getColor(), drawing);
    }
    
    private static String getGradientColor(Color startColor, int row, int col) {
        int r = startColor.getRed() + row + col;
        int g = startColor.getGreen() + row + col;
        int b = startColor.getBlue() + row + col;
        //This would add star emojis, but they are not monospace so they ruin the egg's form:
//        int rnd = (int) Math.round(Math.random() * 100);
//        
//        if (rnd == 1) {
//            return "✨";
//        }

        return "\u001b[38;2;" + r + ";" + g + ";" + b + "m█";
    }

    /**
     * @return amount of days until easter
     * Inspired by https://coderanch.com/t/697053/java/countdown-date-Java-application
     */
    private static long getDaysUntilEaster() {
        Calendar easter = Calendar.getInstance();
        easter.set(Calendar.DATE, 5);
        easter.set(Calendar.MONTH, 3);
        Calendar today = Calendar.getInstance();
        long diff =  easter.getTimeInMillis() - today.getTimeInMillis();
        if (diff < 0) {
            easter.set(Calendar.YEAR, today.get(Calendar.YEAR) + 1);
            diff =  easter.getTimeInMillis() - today.getTimeInMillis();
        }
        long diffSec = diff / 1000;
        return diffSec / SECONDS_IN_A_DAY;
    }
}
