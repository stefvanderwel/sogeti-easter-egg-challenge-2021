package nl.sogeti.com;

import java.awt.*;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;

public class Text {
    public final static List<AbstractMap.SimpleEntry<String, List<List<Boolean>>>> textGraphics = List.of(
            new AbstractMap.SimpleEntry<>("0", List.of(
                    List.of(true, true, true),
                    List.of(true, false, true),
                    List.of(true, false, true),
                    List.of(true, false, true),
                    List.of(true, true, true)
            )),
            new AbstractMap.SimpleEntry<>("1", List.of(
                    List.of(false, true, false),
                    List.of(false, true, false),
                    List.of(false, true, false),
                    List.of(false, true, false),
                    List.of(false, true, false)
            )),
            new AbstractMap.SimpleEntry<>("2", List.of(
                    List.of(true, true, true),
                    List.of(false, false, true),
                    List.of(true, true, true),
                    List.of(true, false, false),
                    List.of(true, true, true)
            )),
            new AbstractMap.SimpleEntry<>("3", List.of(
                    List.of(true, true, true),
                    List.of(false, false, true),
                    List.of(true, true, true),
                    List.of(false, false, true),
                    List.of(true, true, true)
            )),
            new AbstractMap.SimpleEntry<>("4", List.of(
                    List.of(true, false, true),
                    List.of(true, false, true),
                    List.of(true, true, true),
                    List.of(false, false, true),
                    List.of(false, false, true)
            )),
            new AbstractMap.SimpleEntry<>("5", List.of(
                    List.of(true, true, true),
                    List.of(true, false, false),
                    List.of(true, true, true),
                    List.of(false, false, true),
                    List.of(true, true, true)
            )),
            new AbstractMap.SimpleEntry<>("6", List.of(
                    List.of(true, true, true),
                    List.of(true, false, false),
                    List.of(true, true, true),
                    List.of(true, false, true),
                    List.of(true, true, true)
            )),
            new AbstractMap.SimpleEntry<>("7", List.of(
                    List.of(true, true, true),
                    List.of(false, false, true),
                    List.of(false, false, true),
                    List.of(false, false, true),
                    List.of(false, false, true)
            )),
            new AbstractMap.SimpleEntry<>("8", List.of(
                    List.of(true, true, true),
                    List.of(true, false, true),
                    List.of(true, true, true),
                    List.of(true, false, true),
                    List.of(true, true, true)
            )),
            new AbstractMap.SimpleEntry<>("9", List.of(
                    List.of(true, true, true),
                    List.of(true, false, true),
                    List.of(true, true, true),
                    List.of(false, false, true),
                    List.of(true, true, true)
            )),
            new AbstractMap.SimpleEntry<>("D", List.of(
                    List.of(true, true, false),
                    List.of(true, false, true),
                    List.of(true, false, true),
                    List.of(true, false, true),
                    List.of(true, true, false)
            )),
            new AbstractMap.SimpleEntry<>("A", List.of(
                    List.of(false, true, false),
                    List.of(true, false, true),
                    List.of(true, false, true),
                    List.of(true, true, true),
                    List.of(true, false, true)
            )),
            new AbstractMap.SimpleEntry<>("Y", List.of(
                    List.of(true, false, true),
                    List.of(true, false, true),
                    List.of(true, true, true),
                    List.of(false, true, false),
                    List.of(false, true, false)
            )),
            new AbstractMap.SimpleEntry<>("S", List.of(
                    List.of(true, true, true),
                    List.of(true, false, false),
                    List.of(true, true, true),
                    List.of(false, false, true),
                    List.of(true, true, true)
            )),
            new AbstractMap.SimpleEntry<>("°", List.of(
                    List.of(true, true, true),
                    List.of(true, false, true),
                    List.of(true, true, true),
                    List.of(false, false, false),
                    List.of(false, false, false)
            ))
            
    );

    public static List<AbstractMap.SimpleEntry<Point, String>> getTextForNumber(String text, Point coords, Color color) {
        String[] numberStrings = text.split("");
        List<AbstractMap.SimpleEntry<Point, String>> result = new ArrayList<>();
        for (int i = 0; i < numberStrings.length; i++) {
            String numberString = numberStrings[i];
            AbstractMap.SimpleEntry<String, List<List<Boolean>>> stringTextCoords = textGraphics.stream().filter(entry -> entry.getKey().equals(numberString)).findFirst().get();
            for (int j = 0; j < stringTextCoords.getValue().size(); j++) {
                List<Boolean> booleans = stringTextCoords.getValue().get(j);
                for (int i1 = 0; i1 < booleans.size(); i1++) {
                    int x = coords.x + i1 + i * 4;
                    int y = coords.y + j;
                    Boolean aBoolean = booleans.get(i1);
                    if (aBoolean) {
                        result.add(new AbstractMap.SimpleEntry<>(new Point(x, y), "\u001b[38;2;" + color.getRed() + ";" + color.getGreen() + ";" + color.getBlue() + "m█"));
                    }
                }
            }
        }
        return result;
    }
}
