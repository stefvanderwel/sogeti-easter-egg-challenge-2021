# Sogeti Easter competition

Hi, thanks for checking out my entry for the Easter egg competition!

This egg has a gradient background and counts down the amount of days until Easter 😁

Example: 

![example](src/images/example.png)

I hope you like it!

Kind regards,

Stef
